# SuperNET Lite #

The SuperNET Lite Client runs with opening a html file on your Computer. No other setups needed.

Use BTC, LTC, BTCDark, NXT, DOGE, VERICCOIN, BITSTAR and OPAL from the LITE Client. 

We hope to make all unique features from different currencies usable in an easy way.

### What is this repository for? ###

* Improving the Liteclient
* 2.0.6


### How do I get set up? ###

* Download and open the supernet.html file with your Browser.
* Create your Account and receive your deposit addresses, start using.

### Contribution guidelines ###

* Code review
* Additions

### Who do I talk to? ###

* Tosch110
* Other community or team contact